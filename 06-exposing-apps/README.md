Exposing apps
=============

After finishing this notebook, you'll learn:
* How to expose a pod's open port

## Overview

Kubernetes documentation has a complete section dedicated to
[exposing apps](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/).

## Excercise

TODO exposing apps - services, loadbalancer, nodeport, ingress, create each service, see how to access the app, try to break it (change app port), choose a privileged/already used port

```bash
# apply a deployment with a wrong configmap mounted
kubectl create -f 2048_full.yaml
# check logs
kubectl logs deploy/deployment-2048
# see services
kubectl get svc
# see ingress controllers
kubectl get ingress
```

