#!/bin/bash

TAG=nineinchnick/config-printer:0.1

docker build --platform linux/amd64 --tag $TAG-amd64 .
docker push $TAG-amd64
docker build --platform linux/arm64 --tag $TAG-arm64 .
docker push $TAG-arm64

docker manifest create $TAG $TAG-amd64 $TAG-arm64
docker manifest push $TAG

kind load docker-image $TAG


TAG=nineinchnick/config-printer:0.2

docker build --platform linux/amd64 --tag $TAG-amd64 -f Dockerfile2 .
docker push $TAG-amd64

docker build --platform linux/arm64 --tag $TAG-arm64 -f Dockerfile2 .
docker push $TAG-arm64

docker manifest create $TAG $TAG-amd64 $TAG-arm64
docker manifest push $TAG

kind load docker-image $TAG
