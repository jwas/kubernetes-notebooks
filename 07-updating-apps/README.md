Updating apps
=============

After finishing this notebook, you'll learn:
* How apps can be updated without users noticing

## Overview

Kubernetes documentation has a complete section dedicated to
[updating apps](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/).

## Excercise

Follow the interactive tutorial in the link above.
